package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
    public void testIsValidLoginRegular( ) {
        assertTrue("Invalid Login" , LoginValidator.isValidLoginName("sudikshy"));
    }

    @Test(expected=NumberFormatException.class)
    public void testIsValidLoginException() {
        assertFalse("Invalid Login", LoginValidator.isValidLoginName("sue"));
    }

    @Test
    public void testIsValidLoginBoundaryIn() {
        assertTrue("Invalid Login", LoginValidator.isValidLoginName("sudika"));
    }

    @Test
    public void testIsValidLoginWithAlphaNumbersOnlyBoundaryIn() {
        assertTrue("Invalid Login", LoginValidator.isValidLoginName("sue123"));
    }

    @Test
    public void testIsValidLoginWithShouldNotStartWithNumberBoundaryIn() {
        assertTrue("Invalid Login", LoginValidator.isValidLoginName("s1udik"));
    }

    @Test(expected=NumberFormatException.class)
    public void testIsValidLoginBoundaryOut() {
        assertFalse("Invalid Login", LoginValidator.isValidLoginName("sudi1"));
    }

    @Test(expected=NumberFormatException.class)
    public void testIsValidLoginAlphaAndNumbersOnlyBoundaryOut() {
        assertFalse("Invalid Login", LoginValidator.isValidLoginName("gotr!a"));
    }

    @Test(expected=NumberFormatException.class)
    public void testIsValidLoginShouldNotStartWithNumberBoundaryOut() {
        assertFalse("Invalid Login", LoginValidator.isValidLoginName("1hotra"));
    }

    @Test(expected=NumberFormatException.class)
    public void testIsValidLoginShouldBeAtLeast6AlphaNumericCharsBoundaryOut() {
        assertFalse("Invalid Login", LoginValidator.isValidLoginName("sue12"));
    }


}




